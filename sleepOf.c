#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/time.h>
#include <math.h>

#define DURATION 10
#define STEPS 10
#define MODE 1
#define SIZE 12
//#define SIZE 262144
//#define SIZE 1048576 //2^20
//#define SIZE 4194304 //2^22
#define SIZE 16777216 //2^24
//#define SIZE 67108864 //2^26
//#define SIZE 268435456 // 2^28
//#define SIZE 4294967296 //2^32 ->  MPI_Send(174)................: MPI_Send(buf=0x2af34c9e3860, count=1, dtype=USER<struct>, dest=12, tag=1200, comm=0x84000000) failed
#define MIN 1
#define MAX 20
#define PREF 0

double wall_time() {
    struct timespec ts;
    clock_gettime(CLOCK_MONOTONIC, &ts);
    return (double) (ts.tv_sec) + (double) ts.tv_nsec * 1.0e-9;
}

void solver(int timesteps, int t, int duration) {
    int rank, rank_size, durStep, action, nnodes, factor;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &rank_size);
    MPI_Comm booster;

    durStep = duration / rank_size;
    if (rank == 0)
        printf("(sergio): %s(%s,%d) - Step %d doing a sleep of %d with %d nodes\n", __FILE__, __func__, __LINE__, t, durStep, rank_size);
    sleep(durStep);
    t++;

    if (t < timesteps) {
        solver(timesteps, t, duration);
    }
}

void solverMalleable(double *data, int timesteps, int t, int duration) {
    int rank, rank_size, durStep, action, nnodes, factor, dst;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &rank_size);
    MPI_Comm booster;

     printf("(sergio)[%d/%d] %d: %s(%s,%d)\n", rank, rank_size, getpid(), __FILE__, __func__, __LINE__);
    //const double check1 = wall_time();
    if (rank == 0) {
        double check1 = MPI_Wtime();
        printf("\tTS1: %f\n", check1);
    }
    durStep = duration / rank_size;
    if (rank == 0)
        printf("(sergio): %s(%s,%d) - Step %d doing a sleep of %d with %d nodes\n", __FILE__, __func__, __LINE__, t, durStep, rank_size);
    sleep(durStep);
    t++;
    
    int localSize = SIZE / rank_size;
    /*
    for (int r = 0; r < rank_size; r++) {
        if (r == rank)
            for (int i = 0; i < localSize; i++)
                printf("[%d/%d] - %d: vector[%d] = %.f\n", rank, rank_size, getpid(), i, data[i]);
        MPI_Barrier(MPI_COMM_WORLD);
    }
    */
    if (t < timesteps) {
        const double start = MPI_Wtime();
        action = 0;
        //if (t == 1) {
        action = dmr_check_status(MIN, MAX, 2, PREF, &nnodes, &booster);
        //}
        const double end = MPI_Wtime();
        if (rank == 0)
            printf("CHECK TIME: Resize of step %d: action: %d (from %d to %d) in %g seconds.\n", t, action, rank_size, nnodes, end - start);

        //const double check2 = wall_time();
        if (rank == 0) {
            double check2 = MPI_Wtime();
            printf("\tTS2: %f\n", check2);
        }

        if (action == 1) {
            factor = nnodes / rank_size;
            for (int i = 0; i < factor; i++) {
                dst = rank * factor + i;
                int iniPart = (localSize / factor) * i;
                int finPart = ((localSize / factor) * (i + 1));
#pragma omp task in(data[iniPart:finPart]) onto(booster, dst)
                solverMalleable(data + iniPart, timesteps, t, duration);
            }
#pragma omp taskwait
        } else if (action == 2) {
            factor = rank_size / nnodes;
            int newSize = localSize * factor;
            MPI_Request sendRequest;
            MPI_Status statuses[factor];

            double *newData = malloc(newSize * sizeof (double));

            if ((rank % factor) < (factor - 1)) {
                dst = factor * (rank / factor + 1) - 1;
                MPI_Isend(data, localSize, MPI_DOUBLE, dst, 0, MPI_COMM_WORLD, &sendRequest);
            } else {
                dst = rank;
                MPI_Isend(data, localSize, MPI_DOUBLE, dst, 0, MPI_COMM_WORLD, &sendRequest);
                for (int i = 1; i <= factor; i++) {
                    int src = rank - factor + i;
                    double *ptr = newData + ((i - 1) * localSize);
                    //printf("(sergio)[%d/%d] %d: %s(%s,%d) Send to %d - Recv from %d\n", rank, rank_size, getpid(), __FILE__, __func__, __LINE__, dst, src);
                    MPI_Recv(ptr, localSize, MPI_DOUBLE, src, 0, MPI_COMM_WORLD, &statuses[i - 1]);
                }
            }

            MPI_Barrier(MPI_COMM_WORLD);

            if ((rank % factor) < (factor - 1)) {
            } else {
                int dst = rank / factor;
#pragma omp task in(newData[0:newSize]) onto(booster, dst)
                solverMalleable(newData, timesteps, t, duration);
#pragma omp taskwait
            }
        } else {
            solverMalleable(data, timesteps, t, duration);
        }
    }
}

int main(int argc, char** argv) {
    int provided, i;
    MPI_Init_thread(&argc, &argv, MPI_THREAD_MULTIPLE, &provided);
    int rank, rank_size;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &rank_size);

    int duration = DURATION;
    int mode = MODE;

    if (argc == 3) {
        duration = atoi(argv[1]);
        mode = atoi(argv[2]);
    }

    printf("(sergio): %s(%s,%d) - executing %d steps of %d seconds (mode: %d)\n", __FILE__, __func__, __LINE__, STEPS, duration, mode);

    int localSize = SIZE / rank_size;
    double *vector = malloc(localSize * sizeof (double));

    for (i = 0; i < localSize; i++) {
        vector[i] = localSize * rank + i;
    }

    /*
        for (int r = 0; r < rank_size; r++) {
            if (r == rank)
                for (i = 0; i < localSize; i++)
                    printf("[%d/%d]: vector[%d] = %.f\n", rank, rank_size, i, vector[i]);
            MPI_Barrier(MPI_COMM_WORLD);
        }
        //return 0;
     */
    //printf("(sergio): %s(%s,%d) - executing %d steps of %d seconds\n", __FILE__, __func__, __LINE__, timesteps, duration);
    if (mode == 0)
        solver(STEPS, 0, duration);
    else
        solverMalleable(vector, STEPS, 0, duration);

    free(vector);
    MPI_Finalize();
    return (EXIT_SUCCESS);
}

