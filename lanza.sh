#!/bin/bash

export LD_LIBRARY_PATH=/home/siserte/slurm/lib:/state/partition1/soft/gnu/mpich-3.2/lib:$LD_LIBRARY_PATH
#export NX_ARGS="--enable-block --force-tie-master"

NODELIST="$(scontrol show hostname $SLURM_JOB_NODELIST | paste -d, -s)"
mpiexec -iface eth0 -n $SLURM_JOB_NUM_NODES -hosts $NODELIST ./sleepOf.INTEL64

#multiprocs
#NODELIST="$(python parseCpusPerNode.py)"
#mpiexec -iface eth0 -n $SLURM_NPROCS -hosts $NODELIST ./sleepOf.INTEL64

