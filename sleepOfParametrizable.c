#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/time.h>
#include <math.h>

#define DURATION 0
#define STEPS 2
#define MODE 1
#define MIN 1
#define MAX 20

double wall_time() {
    struct timespec ts;
    clock_gettime(CLOCK_MONOTONIC, &ts);
    return (double) (ts.tv_sec) + (double) ts.tv_nsec * 1.0e-9;
}

void solver(int timesteps, int t, int duration) {
    int rank, rank_size, durStep, action, nnodes, factor;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &rank_size);
    MPI_Comm booster;

    durStep = duration / rank_size;
    if (rank == 0)
        printf("(sergio): %s(%s,%d) - Step %d doing a sleep of %d with %d nodes\n", __FILE__, __func__, __LINE__, t, durStep, rank_size);
    sleep(durStep);
    t++;

    if (t < timesteps) {
        solver(timesteps, t, duration);
    }
}

void solverMalleable(double *data, int timesteps, int t, int duration, int size, int pref) {
    int rank, rank_size, durStep, action, nnodes, factor, dst;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &rank_size);
    MPI_Comm booster;

    //const double check1 = wall_time();
    //if (rank == 0) printf("\tTS1: %f\n", check1);
    if (rank == 0) {
        double check1 = MPI_Wtime();
        printf("\tTS1: %f\n", check1);
    }

    durStep = duration / rank_size;
    if (rank == 0)
        printf("(sergio): %s(%s,%d) - Step %d doing a sleep of %d with %d nodes\n", __FILE__, __func__, __LINE__, t, durStep, rank_size);
    sleep(durStep);
    t++;

    int localSize = size / rank_size;
    /* 
        for (int r = 0; r < rank_size; r++) {
            if (r == rank)
                for (int i = 0; i < localSize; i++)
                    printf("[%d/%d]: vector[%d] = %f\n", rank, rank_size, i, data[i]);
            MPI_Barrier(MPI_COMM_WORLD);
        }
     */
    if (t < timesteps) {
        if (rank == 0) double start = MPI_Wtime();

        action = 0;
        action = dmr_check_status(MIN, MAX, 2, pref, &nnodes, &booster);
        
        //const double check2 = wall_time();
        //if (rank == 0) printf("\tTS2: %f\n", check2);
        if (rank == 0) {
            double check2 = MPI_Wtime();
            printf("CHECK TIME: Resize of step %d: action: %d (from %d to %d) in %g seconds.\n", t, action, rank_size, nnodes, check2 - start);
            printf("\tTS2: %f\n", check2);
        }

        if (action == 1) {
            factor = nnodes / rank_size;
            for (int i = 0; i < factor; i++) {
                dst = rank * factor + i;
                int iniPart = (localSize / factor) * i;
                int finPart = ((localSize / factor) * (i + 1));
#pragma omp task in(data[iniPart:finPart]) onto(booster, dst)
                solverMalleable(data + iniPart, timesteps, t, duration, size, pref);
            }
#pragma omp taskwait
        } else if (action == 2) {
            factor = rank_size / nnodes;
            int newSize = localSize * factor;
            MPI_Request sendRequest;
            MPI_Status statuses[factor];

            double *newData = malloc(newSize * sizeof (double));

            if ((rank % factor) < (factor - 1)) {
                dst = factor * (rank / factor + 1) - 1;
                MPI_Isend(data, localSize, MPI_DOUBLE, dst, 0, MPI_COMM_WORLD, &sendRequest);
            } else {
                dst = rank;
                MPI_Isend(data, localSize, MPI_DOUBLE, dst, 0, MPI_COMM_WORLD, &sendRequest);
                for (int i = 1; i <= factor; i++) {
                    int src = rank - factor + i;
                    double *ptr = newData + ((i - 1) * localSize);
                    //printf("(sergio)[%d/%d] %d: %s(%s,%d) Send to %d - Recv from %d\n", rank, rank_size, getpid(), __FILE__, __func__, __LINE__, dst, src);
                    MPI_Recv(ptr, localSize, MPI_DOUBLE, src, 0, MPI_COMM_WORLD, &statuses[i - 1]);
                }
            }

            MPI_Barrier(MPI_COMM_WORLD);

            if ((rank % factor) < (factor - 1)) {
            } else {
                int dst = rank / factor;
#pragma omp task in(newData[0:newSize]) onto(booster, dst)
                solverMalleable(newData, timesteps, t, duration, size, pref);
#pragma omp taskwait
            }
        } else {
            solverMalleable(data, timesteps, t, duration, size, pref);
        }
    }
}

int main(int argc, char** argv) {
    int provided, i;
    MPI_Init_thread(&argc, &argv, MPI_THREAD_MULTIPLE, &provided);
    int rank, rank_size;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &rank_size);

    int duration = DURATION;
    int mode = MODE;
    int size = -1;
    int pref = -1;

    if (argc == 3) {
        size = atoi(argv[1]);
        pref = atoi(argv[2]);
    }

    if (size == -1 || pref == -1) {
        printf("(sergio): %s(%s,%d) - ERROR en los parámetros de entrada\n", __FILE__, __func__, __LINE__);
        MPI_Finalize();
        return (EXIT_SUCCESS);
    }

    printf("(sergio): %s(%s,%d) - executing %d steps of %d seconds (mode: %d)\n", __FILE__, __func__, __LINE__, STEPS, duration, mode);

    int localSize = size / rank_size;
    double *vector = malloc(localSize * sizeof (double));

    for (i = 0; i < localSize; i++) {
        vector[i] = localSize * rank + i;
    }

    /*
        for (int r = 0; r < rank_size; r++) {
            if (r == rank)
                for (i = 0; i < localSize; i++)
                    printf("[%d/%d]: vector[%d] = %f\n", rank, rank_size, i, vector[i]);
            MPI_Barrier(MPI_COMM_WORLD);
        }
        return 0;
     */
    //printf("(sergio): %s(%s,%d) - executing %d steps of %d seconds\n", __FILE__, __func__, __LINE__, timesteps, duration);
    if (mode == 0)
        solver(STEPS, 0, duration);
    else
        solverMalleable(vector, STEPS, 0, duration, size, pref);

    free(vector);
    MPI_Finalize();
    return (EXIT_SUCCESS);
}

