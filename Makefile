MPIDIR	    = /state/partition1/soft/gnu/mpich-3.2/
SLURMDIR    = /home/siserte/slurm/

MPIFLAGS    = -I/state/partition1/soft/gnu/mpich-3.2/include -L/state/partition1/soft/gnu/mpich-3.2/lib -lmpi
SLURMFLAGS  = -I/home/siserte/slurm/include -L/home/siserte/slurm/lib -lslurm 
OMPSSFLAGS  = -O3 -k --ompss

#MPIFLAGS    = -I/home/bsc15/bsc15334/apps/install/mpich-3.2/include -L/home/bsc15/bsc15334/apps/install/mpich-3.2/lib -lmpi
#SLURMFLAGS  = -I/home/bsc15/bsc15334/apps/install/slurm/include -L/home/bsc15/bsc15334/apps/install/slurm/lib -lslurm
     
#all: sleep-malleable sleep-param
all: sleep-malleable
	
######### O (N^2) computation	##########
sleep-param: sleepOfParametrizable.c
	mpimcc $(OMPSSFLAGS) $(MPIFLAGS) $(SLURMFLAGS) sleepOfParametrizable.c -o sleepOfParametrizable.INTEL64
	
sleep-malleable: sleepOf.c
	mpimcc $(OMPSSFLAGS) $(MPIFLAGS) $(SLURMFLAGS) sleepOf.c -o sleepOf.INTEL64
	
clean:
	rm -f *.out *.o *.INTEL64 core.* mpimcc_mcc*
