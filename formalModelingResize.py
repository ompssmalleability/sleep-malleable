# coding: latin1

import os, sys, time
import subprocess
import numpy

#### PARA QUE FUNCIONA TIENE QUE HABER UN TRABAJO EN EJECUCIÓN EN LA COLA (ESTAMOS UTILIZANDO EL PREFERRED)
####
#### Además podemos redirigir la salida de error >2 para ver el resultado mejor
REPS = 10
SIZES = [1048576, 4194304, 16777216, 67108864]
SIZES = [67108864]
PROCS = [1, 2, 4, 8, 16]
#PROCS = [1, 8]

cmd1 = "make -j8"
#subprocess.call(cmd1.split(), shell=True)

for size in SIZES: 
	### expand test
	for p in PROCS[1:]:
		ini = PROCS[0]
		resizeTimes = []
		scheduleTimes = []
		print "Expand: Size %d, from %d to %d" % (size, ini, p)
		rep = 0
		while rep < REPS:
			#print "Test %d: Size %d, from %d to %d" % (rep, size, ini, p)
			cmd1 = "salloc -N%d ./lanzaParam.sh %d %d" % (ini, size, p)
			p1 = subprocess.Popen(cmd1.split(), stdout=subprocess.PIPE)
			#print p1.communicate()[0].split("\n")
			#sys.exit(0)
			cmd2 = "grep -e TS -e CHECK"
			#print cmd2
			p2 = subprocess.Popen(cmd2.split(), stdin=p1.stdout, stdout=subprocess.PIPE)
			p1.stdout.close()
			output = p2.communicate()[0].split("\n")
			#print output
			sched = float(output[1].split()[13])
			resiz = float(output[3].split()[1]) - float(output[2].split()[1])
			print "Sched: %f - Resiz: %f" % (sched, resiz)
			#if (resiz < 0 or resiz > 50):
			#	pass
			#else:
				#print "Sched: %f - Resiz: %f" % (sched, resiz)
			scheduleTimes.append(sched)
			resizeTimes.append(resiz)	
			rep += 1
			#time.sleep(2)
		print "Schedule time: %f" % numpy.mean(scheduleTimes)
		print "Resize time: %f" % numpy.mean(resizeTimes)
		#sys.exit(0)
	### shrink test
	for p in PROCS[:-1]:
		ini = PROCS[-1]
		resizeTimes = []
		scheduleTimes = []
		print "Shrink: Size %d, from %d to %d" % (size, ini, p)
		for rep in xrange(REPS):
			#print "Test %d: Size %d, from %d to %d" % (rep, size, ini, p)
			cmd1 = "salloc -N%d ./lanzaParam.sh %d %d" % (ini, size, p)
			p1 = subprocess.Popen(cmd1.split(), stdout=subprocess.PIPE)
			#print p1.communicate()[0].split("\n")
			#sys.exit(0)
			cmd2 = "grep -e TS -e CHECK"
			#print cmd2
			p2 = subprocess.Popen(cmd2.split(), stdin=p1.stdout, stdout=subprocess.PIPE)
			p1.stdout.close()
			output = p2.communicate()[0].split("\n")
			scheduleTimes.append(float(output[1].split()[13]))
			resizeTimes.append((float(output[3].split()[1]) - float(output[2].split()[1])))
		print "Schedule time: %f" % numpy.mean(scheduleTimes)
		print "Resize time: %f" % numpy.mean(resizeTimes)
